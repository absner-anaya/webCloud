import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthSocialNetworksComponent } from './components/auth-social-networks/auth-social-networks.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'Dashboard/access',
    pathMatch: 'full'
  },
  {
    path: 'Dashboard',
    redirectTo: 'Dashboard/access'
  },
  {
    path: 'Dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'access',
        component: AuthSocialNetworksComponent
      },
      {
        path: 'access/Gplus/:id',
        component: AuthSocialNetworksComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
