export interface IConnectGplus {
    El?: String;
    tokens?: {
        access_toke?: String;
        expirate_at?: Number;
        expirate_in?: Number;
        first_issued_at?: Number;
        id_token?: String;
        idpId?: String;
        login_hint?: String;
        scope?: String;
        session_state?: {
            extraQueryParams?: {
                authuser?: String;
            }
        };
        token_type?: String;
    };
    data?:  {
        Eea?: String;
        Paa?: String;
        U3?: String;
        ig?: String;
        ofa?: String;
        wea?: String;
    };
}

export class ConnectGplus {
    El?: String;
    tokens?: {
        access_toke?: String;
        expirate_at?: Number;
        expirate_in?: Number;
        first_issued_at?: Number;
        id_token?: String;
        idpId?: String;
        login_hint?: String;
        scope?: String;
        session_state?: {
            extraQueryParams?: {
                authuser?: String;
            }
        };
        token_type?: String;
    };
    data?:  {
        Eea?: String;
        Paa?: String;
        U3?: String;
        ig?: String;
        ofa?: String;
        wea?: String;
    };
}
