import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthSocialNetworksComponent } from './auth-social-networks.component';

describe('AuthSocialNetworksComponent', () => {
  let component: AuthSocialNetworksComponent;
  let fixture: ComponentFixture<AuthSocialNetworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthSocialNetworksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthSocialNetworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
