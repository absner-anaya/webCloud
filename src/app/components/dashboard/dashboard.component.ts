import { Component, ElementRef, Renderer2, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public is_toogle: Boolean = false;
  public group_name_open: any;

  @ViewChild('Toogle') Toogle: ElementRef;
  @ViewChild('Content') Content: ElementRef;

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
  }

  isOpen(menu_id_: string): boolean {
    if (this.group_name_open === menu_id_) {
        return true;
    }
    return false;
  }
  openMenuGroup(menu_id_: string): void {
    if (this.group_name_open === menu_id_) {
        this.group_name_open = null;
    }else {
        this.group_name_open = menu_id_;
    }
  }

/**
 * toogle
 */
public toogle() {
  this.is_toogle  = !this.is_toogle;
  if (this.is_toogle) {
    this.renderer.removeClass(this.Toogle.nativeElement, 'lateral-menu');
    this.renderer.removeClass(this.Content.nativeElement, 'content-option');
    this.renderer.addClass(this.Toogle.nativeElement, 'lateral-menu-mini');
    this.renderer.addClass(this.Content.nativeElement, 'content-option-full');
  }else {
    this.renderer.removeClass(this.Content.nativeElement, 'content-option-full');
    this.renderer.addClass(this.Content.nativeElement, 'content-option');
    this.renderer.removeClass(this.Toogle.nativeElement, 'lateral-menu-mini');
    this.renderer.addClass(this.Toogle.nativeElement, 'lateral-menu');
  }
}

}
